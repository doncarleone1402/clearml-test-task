def download_data(pickle_data_url):
    import sklearn  # noqa
    import pickle
    import pandas as pd
    from clearml import StorageManager
    pickle_data_url = \
        pickle_data_url or \
        'https://github.com/allegroai/events/raw/master/odsc20-east/generic/iris_dataset.pkl'
    local_iris_pkl = StorageManager.get_local_copy(remote_url=pickle_data_url)
    with open(local_iris_pkl, 'rb') as f:
        iris = pickle.load(f)
    data_frame = pd.DataFrame(iris['data'], columns=iris['feature_names'])
    data_frame.columns += ['target']
    data_frame['target'] = iris['target']
    return data_frame


def data_split(data_frame, test_size=0.2, random_state=42):
    import pandas as pd  # noqa
    from sklearn.model_selection import train_test_split
    y = data_frame['target']
    X = data_frame[(c for c in data_frame.columns if c != 'target')]
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=test_size, random_state=random_state)

    return X_train, X_test, y_train, y_test


def model_training(data):
    import pandas as pd  # noqa
    from sklearn.linear_model import LogisticRegression
    from clearml import PipelineController
    import pickle

    X_train, X_test, y_train, y_test = data
    model = LogisticRegression(solver='liblinear', multi_class='auto')
    model.fit(X_train, y_train)

    with open("/model.pkl", 'wb') as modelfile:
        pickle.dump(model, modelfile)

    PipelineController.upload_artifact(
        name="LogReg_model",
        artifact_object="/model.pkl"
    )

    PipelineController.upload_artifact(
        name="eval_data",
        artifact_object=(X_test, y_test)
    )

    return X_test, y_test


def model_evaluation(data):
    from clearml import PipelineController
    import pickle

    modelname = PipelineController._get_pipeline_task().artifacts[
        f'LogReg_model'
    ].get_local_copy()

    X_test, y_test = data

    model = pickle.load(open(modelname, 'rb'))

    accuracy = model.score(X_test, y_test)

    print(f"MEAN ACCURACY: {accuracy}")

    return None

def model_uploading():
    from clearml import PipelineController, OutputModel, Task

    modelname = PipelineController._get_pipeline_task().artifacts[
        f'LogReg_model'
    ].get_local_copy()

    out_model = OutputModel(
        task=Task.current_task(),
        name='logreg',
        framework='sklearn'
    )
    out_model.set_upload_destination("https://files.clear.ml")
    out_model.update_weights(weights_filename=modelname)

    return None


if __name__ == '__main__':
    import os
    from clearml import PipelineController

    # Твой путь до clearml конфига (clearml.conf) - нужно поставить правильный
    os.environ["CLEARML_CONFIG_FILE"] = "/path/to/config"


    dockerfile = "python:3.10-slim-buster"
    queue = "default"
    proj_name = 'Test project'
    requirements_path = "./requirements_clearml.txt"

    # Инициализируешь PipelineController с нужными параметрами

    # Инициализируешь параметр пайплайна

    # Инициализируешь шаги пайплайна по очереди
